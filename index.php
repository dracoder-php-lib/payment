<?php

require('autoload.php');
require('vendor/autoload.php');

use Dracoder\PaymentBundle\Tests\TestController;

$action = filter_input(INPUT_GET, 'action');
$testController = new TestController();

if ($action) {
    if ($action === 'create') {
        $testController->createPayment();
        die();
    }
    if ($action === 'confirmed') {
        $testController->confirmed();
        die();
    }
    if ($action === 'canceled') {
        $testController->canceled();
        die();
    }
    if ($action === 'refund-uncompleted') {
        $testController->refundUncompleted();
        die();
    }
}
die('select action: ?action=(refund-uncompleted|create|confirmed|canceled)');
