<?php

namespace Dracoder\PaymentBundle\Service;


use Dracoder\PaymentBundle\Model\BuyerInterface;
use Dracoder\PaymentBundle\Model\SellerInterface;

interface GatewayInterface
{
    public function __construct(ManagerInterface $manager);

    /**
     * @param BuyerInterface $buyer
     * @param SellerInterface $seller
     * @param float $quantity
     * @param string $subject
     * @param string $currency
     *
     * @return bool|null
     */
    public function pay(
        BuyerInterface $buyer,
        SellerInterface $seller,
        float $quantity,
        string $subject,
        string $currency
    ): ?bool;
}
