<?php

namespace Dracoder\PaymentBundle\Service;

use Dracoder\PaymentBundle\Model\PayableInterface;
use Dracoder\PaymentBundle\Model\PayableTokenizerInterface;

abstract class AbstractTwoStepPaymentGateway implements TwoStepPaymentGatewayInterface
{
    protected ManagerInterface $manager;
    protected PayableTokenizerInterface $payableTokenizer;
    protected PayableValidator $payableValidator;

    /**
     * @param ManagerInterface $manager
     * @param PayableTokenizerInterface $payableTokenizer
     */
    public function __construct(
        ManagerInterface $manager,
        PayableTokenizerInterface $payableTokenizer
    ) {
        $this->manager = $manager;
        $this->payableTokenizer = $payableTokenizer;
        $this->payableValidator = new PayableValidator($payableTokenizer);
    }

    /**
     * @param PayableInterface $payable
     * @param string $subject
     * @param string $confirmedCallback
     * @param string $deniedCallback
     * @param string $currency
     *
     * @return string|null
     */
    abstract public function initPayment(
        PayableInterface $payable,
        string $subject,
        string $confirmedCallback,
        string $deniedCallback,
        string $currency = 'EUR'
    ): ?string;

    /**
     * @param PayableInterface $payable
     * @param string $token
     *
     * @return bool
     */
    abstract public function finishPayment(PayableInterface $payable, string $token): bool;

    /**
     * @param PayableInterface $payable
     *
     * @return string|null
     */
    public function getConfirmedToken(PayableInterface $payable): string
    {
        return $this->payableTokenizer->getConfirmedToken($payable);
    }

    /**
     * @param PayableInterface $payable
     *
     * @return string|null
     */
    public function getDeniedToken(PayableInterface $payable): string
    {
        return $this->payableTokenizer->getDeniedToken($payable);
    }
}
