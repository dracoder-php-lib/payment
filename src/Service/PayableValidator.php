<?php

namespace Dracoder\PaymentBundle\Service;

use Dracoder\PaymentBundle\Model\PayableInterface;
use Dracoder\PaymentBundle\Model\PayableTokenizerInterface;
use Dracoder\PaymentBundle\Model\PaymentStatus;

class PayableValidator
{
    /** @var PayableTokenizerInterface $payableTokenizer */
    private PayableTokenizerInterface $payableTokenizer;

    public function __construct(PayableTokenizerInterface $payableTokenizer)
    {
        $this->payableTokenizer = $payableTokenizer;
    }

    /**
     * @param PayableInterface $payable
     * @param string $token
     *
     * @return bool payable is valid
     */
    public function validate(PayableInterface $payable, string $token): bool
    {
        $isValid = false;
        $salt = $payable->getSalt();
        if (!$salt) {
            $isValid = true;
        } elseif ($this->payableTokenizer->getConfirmedToken($payable) === $token) {
            $isValid = true;
        } elseif ($this->payableTokenizer->getDeniedToken($payable) === $token) {
            $payable->setPaymentStatus(PaymentStatus::CANCELED);
        }

        if ($isValid) {
            $payable->setPaymentStatus(PaymentStatus::PAID);

            return true;
        }

        return false;
    }
}
