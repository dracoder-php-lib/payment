<?php

namespace Dracoder\PaymentBundle\Service;

use Dracoder\PaymentBundle\Model\PayableInterface;
use Dracoder\PaymentBundle\Model\PayableTokenizerInterface;

interface TwoStepPaymentGatewayInterface
{
    public function __construct(ManagerInterface $manager, PayableTokenizerInterface $payableTokenizer);

    /**
     * @param PayableInterface $payable
     * @param string $subject
     * @param string $confirmedCallback
     * @param string $deniedCallback
     * @param string $currency
     *
     * @return string|null
     */
    public function initPayment(
        PayableInterface $payable,
        string $subject,
        string $confirmedCallback,
        string $deniedCallback,
        string $currency = 'EUR'
    ): ?string;

    /**
     * @param PayableInterface $payable
     * @param string $token
     *
     * @return bool
     */
    public function finishPayment(PayableInterface $payable, string $token): bool;
}
