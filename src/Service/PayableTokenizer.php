<?php

namespace Dracoder\PaymentBundle\Service;

use Dracoder\PaymentBundle\Model\PayableInterface;
use Dracoder\PaymentBundle\Model\PayableTokenizerInterface;

class PayableTokenizer implements PayableTokenizerInterface
{
    private string $confirmedSecret;
    private string $deniedSecret;

    /**
     * PayableTokenizer constructor.
     *
     * @param string $confirmedSecret
     * @param string $deniedSecret
     */
    public function __construct(string $confirmedSecret, string $deniedSecret)
    {
        $this->confirmedSecret = $confirmedSecret;
        $this->deniedSecret = $deniedSecret;
    }

    /**
     * @param PayableInterface $payable
     *
     * @return string
     */
    public function getConfirmedToken(PayableInterface $payable): string
    {
        $payableId = $payable->getId();
        $timestamp = $payable->getCreatedAt() ? $payable->getCreatedAt()->getTimestamp() : '';
        $salt = $payable->getSalt() ?: '';

        return $this->encode($payableId.$timestamp.$this->confirmedSecret.$salt.'+');
    }

    /**
     * @param PayableInterface $payable
     *
     * @return string
     */
    public function getDeniedToken(PayableInterface $payable): string
    {
        $payableId = $payable->getId();
        $timestamp = $payable->getCreatedAt() ? $payable->getCreatedAt()->getTimestamp() : '';
        $salt = $payable->getSalt() ?: '';

        return $this->encode($payableId.$timestamp.$this->deniedSecret.$salt.'-');
    }

    /**
     * @param string $algo
     *
     * @return string
     */
    private function encode(string $algo): string
    {
        return md5($algo);
    }
}
