<?php

namespace Dracoder\PaymentBundle\Service\Truust;

use DateTime;
use Dracoder\PaymentBundle\Model\PaymentStatus;
use Dracoder\PaymentBundle\Service\ManagerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;

class TruustManager implements ManagerInterface
{
    private const API_DEBUG = 'https://api-sandbox.truust.io/2.0/';
    private const API_PROD = 'https://api.truust.io/2.0/';
    private const PAYMENT_ACCEPTED_STATUS = 'CONFIRMED';
    private string $privateKey;
    private Client $client;

    /**
     * TruustManager constructor.
     *
     * @param string $privateKey
     * @param bool $isDebug
     */
    public function __construct(string $privateKey, bool $isDebug = false)
    {
        $this->privateKey = $privateKey;
        if ($isDebug) {
            $this->client = new Client(['base_uri' => self::API_DEBUG, 'verify' => false]);
        } else {
            $this->client = new Client(['base_uri' => self::API_PROD]);
        }
    }

    /**
     * @param string $buyerId
     * @param string $sellerId
     * @param float $quantity
     * @param string $subject
     * @param string $confirmedCallback
     * @param string $deniedCallback
     *
     * @return string|null
     */
    public function createOrder(
        string $buyerId,
        string $sellerId,
        float $quantity,
        string $subject,
        string $confirmedCallback,
        string $deniedCallback
    ): ?string {
        return $this->sendRequest(
            TruustRouter::getRoute(TruustRouter::ROUTE_ORDER_CREATE),
            [
                'name' => $subject,
                'buyer_id' => $buyerId,
                'seller_id' => $sellerId,
                'value' => $quantity,
                'buyer_confirmed_url' => $confirmedCallback,
                'buyer_denied_url' => $deniedCallback,
                'fee_value' => 0,
                'fee_percent' => 0,
            ],
            'id'
        );
    }

    /**
     * @param array $route
     * @param array $body
     * @param string|null $return
     *
     * @return mixed
     */
    public function sendRequest(array $route, array $body = [], ?string $return = null)
    {
        try {
            $response = $this->client->request(
                $route['method'],
                $route['url'],
                [
                    'headers' => $this->getHeaders(),
                    'form_params' => $body,
                ]
            );

            if ($response) {
                $data = json_decode(
                    $this->cleanJson($response->getBody()->__toString()),
                    true,
                    512,
                    JSON_THROW_ON_ERROR
                );
                if ($return) {
                    return $data['data'][$return] ?? null;
                }

                return $data;
            }
            error_log($route['url'] . ' - No response');
        } catch (GuzzleException $e) {
            error_log($route['url'] . ' - (GuzzleException) ' . $e->getMessage());
        } catch (JsonException $e) {
            error_log($route['url'] . ' - (JsonException) ' . $e->getMessage());
        }

        return null;
    }

    /**
     * @param array $append
     *
     * @return array
     */
    private function getHeaders(array $append = []): array
    {
        return array_merge(
            [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $this->privateKey,
            ],
            $append
        );
    }

    /**
     * @param string $jsonString
     *
     * @return string
     */
    private function cleanJson(string $jsonString): string
    {
        return preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $jsonString);
    }

    /**
     * @param string $customerId
     * @param string $currency
     *
     * @return string|null
     */
    public function createWallet(string $customerId, string $currency): ?string
    {
        return $this->sendRequest(
            TruustRouter::getRoute(TruustRouter::ROUTE_WALLET_CREATE),
            [
                'customer_id' => $customerId,
                'currency' => $currency,
            ],
            'id'
        );
    }

    /**
     * @param string $email
     *
     * @return string
     */
    public function createCustomer(string $email): ?string
    {
        return $this->sendRequest(TruustRouter::getRoute(TruustRouter::ROUTE_CUSTOMER_CREATE), ['email' => $email], 'id');
    }

    /**
     * @param string $orderId
     *
     * @return bool
     */
    public function acceptOrder(string $orderId): bool
    {
        $response = $this->sendRequest(TruustRouter::getRoute(TruustRouter::ROUTE_ORDER_ACCEPT, ['id' => $orderId]), [], 'id');

        return (bool) $response;
    }

    /**
     * @param string $orderId
     * @param string $type
     * @param string|null $paymentId
     *
     * @return string|null
     */
    public function createPayIn(
        string $orderId,
        string $type = PaymentMethod::PAYMENT_METHOD_REDSYS,
        ?string $paymentId = null
    ): ?string {
        $body = PaymentMethod::getPayInBody($orderId, $type, $paymentId);
        if ($body) {
            return $this->sendRequest(TruustRouter::getRoute(TruustRouter::ROUTE_PAYIN_CREATE), $body, 'direct_link');
        }

        return null;
    }

    /**
     * @param string $payInId
     *
     * @return bool
     */
    public function isPayInCompleted(string $payInId): bool
    {
        $status = $this->sendRequest(TruustRouter::getRoute(TruustRouter::ROUTE_PAYIN_SHOW, ['id' => $payInId]), [], 'status');

        return ($status === self::PAYMENT_ACCEPTED_STATUS);
    }

    /**
     * @param string $orderId
     * @param string $type
     * @param string|null $paymentId
     *
     * @return string
     */
    public function createPayOut(
        string $orderId,
        string $type = PaymentMethod::PAYMENT_METHOD_WALLET,
        ?string $paymentId = null
    ): ?string {
        $body = PaymentMethod::getPayOutBody($orderId, $type, $paymentId);
        if ($body) {
            return $this->sendRequest(TruustRouter::getRoute(TruustRouter::ROUTE_PAYOUT_CREATE), $body, 'id');
        }

        return null;
    }

    /**
     * @param string $orderId
     *
     * @return bool
     */
    public function validateOrder(string $orderId): bool
    {
        $response = $this->sendRequest(TruustRouter::getRoute(TruustRouter::ROUTE_ORDER_VALIDATE, ['id' => $orderId]), [], 'id');

        return (bool) $response;
    }

    /**
     * @param string $date
     *
     * @return array|null
     */
    public function getOrdersByDay(string $date): ?array
    {
        $dateBegin = urlencode($date . ' 00:00:00');
        $dateEnd = urlencode($date . ' 23:59:59');

        return $this->getOrders(
            '?search=created_at&date_begin=' . $dateBegin . '&date_end=' . $dateEnd
        );
    }

    /**
     * @param string $search
     *
     * @return array
     */
    public function getOrders(string $search = ''): ?array
    {
        $orders = [];
        $response = $this->sendRequest(TruustRouter::getRoute(TruustRouter::ROUTE_ORDER_LIST, ['search' => $search]));
        if ($response !== null && isset($response['data'])) {
            foreach ($response['data'] as $order) {
                $orders[] = $order;
            }
            $lastPage = ($response && isset($response['data']['meta']['last_page'])) ? $response['data']['meta']['last_page'] : null;
            for ($page = 2; $page <= $lastPage; $page++) {
                $nextPage = $search . '&page=' . $page;
                $response = $this->sendRequest(TruustRouter::getRoute(TruustRouter::ROUTE_ORDER_LIST, ['search' => $nextPage]));
                if ($response !== null && isset($response['data'])) {
                    foreach ($response['data'] as $order) {
                        $orders[] = $order;
                    }
                }
            }

            return $orders;
        }

        return null;
    }

    /**
     * @param string $orderId
     * 
     * @return array|null
     */
    public function getOrder(string $orderId): ?array
    {
        return $this->sendRequest(TruustRouter::getRoute(TruustRouter::ROUTE_ORDER_SHOW, ['id' => $orderId]));
    }

    /**
     * @param string $customerId
     * @param string $customerName
     * @param string $accountNumber
     * @param string $bic
     * @param string $accountType
     *
     * @return string|null
     */
    public function createBankAccount(string $customerId, string $customerName, string $accountNumber, string $bic, string $accountType = 'IBAN'): ?string
    {
        return $this->sendRequest(
            TruustRouter::getRoute(TruustRouter::ROUTE_BANK_ACCOUNT_CREATE),
            [
                'customer_id' => $customerId,
                'number' => $accountNumber,
                'bic' => $bic,
                'name' => $customerName,
                'type' => $accountType,
            ],
            'id'
        );
    }

    /**
     * @param string $bankAccountId
     * 
     * @return arrray|null
     */
    public function getBankAccount(string $bankAccountId): ?array
    {
        $response = $this->sendRequest(TruustRouter::getRoute(TruustRouter::ROUTE_BANK_ACCOUNT_SHOW, ['id' => $bankAccountId]));

        return $response ? $response['data'] : null;
    }

    /**
     * @param string $customerId
     * 
     * @return arrray
     */
    public function getCustomerBankAccounts(string $customerId): ?array
    {
        $response = $this->sendRequest(TruustRouter::getRoute(TruustRouter::ROUTE_CUSTOMER_BANKS, ['id' => $customerId]));

        return $response ? $response['data'] : [];
    }

    /**
     * @param string $customerId
     * @param string $customerName
     * @param string $accountNumber
     * @param string $bic
     * @param string $accountType
     *
     * @return array|null
     */
    public function getOrCreateBankAccount(
        string $customerId,
        string $customerName,
        string $accountNumber,
        string $bic,
        string $accountType = 'IBAN'
    ): ?array {
        $bankAccounts = $this->getCustomerBankAccounts($customerId);
        $bankAccountsFiltered = array_filter($bankAccounts, function ($bankAccountItem) use ($customerName, $accountNumber, $bic, $accountType) {
            return
                $bankAccountItem['type'] === $accountType &&
                $bankAccountItem['name'] === $customerName &&
                $bankAccountItem['number'] === $accountNumber &&
                $bankAccountItem['bic'] === $bic;
        });

        $bankAccountFounded = count($bankAccountsFiltered) > 0 ? $bankAccountsFiltered[0] : null;

        if ($bankAccountFounded) {
            return $bankAccountFounded;
        }

        $bankAccountIdCreated = $this->createBankAccount($customerId, $customerName, $accountNumber, $bic, $accountType);

        if (!$bankAccountIdCreated) {
            return null;
        }

        return $this->getBankAccount($bankAccountIdCreated);
    }

    /**
     * @param array $order
     *
     * @return string|null
     */
    public function markOrderAsPaid(array $order): ?string
    {
        return $this->sendRequest(
            TruustRouter::getRoute(TruustRouter::ROUTE_ORDER_UPDATE, ['id' => $order['id']]),
            [
                'name' => $order['name'],
                'value' => $order['value'],
                'metadata[paid]' => 'true',
                'metadata[wc_pub]' => $order['metadata']['wc_pub'],
                'metadata[parent_truust_order]' => $order['metadata']['parent_truust_order'],
                'metadata[wc_order]' => $order['metadata']['wc_order'],
            ]
        );
    }

    /**
     * @param string $payInId
     *
     * @return string|null
     */
    public function refundPayIn(string $payInId): ?string
    {
        $response = $this->sendRequest(TruustRouter::getRoute(TruustRouter::ROUTE_PAYIN_REFUND, ['id' => $payInId]), [], 'id');

        return (bool) $response;
    }

    /**
     * @param string $orderId
     *
     * @return array
     */
    public function getOrderPayInIds(string $orderId): array
    {
        $data = $this->sendRequest(TruustRouter::getRoute(TruustRouter::ROUTE_ORDER_PAYINS, ['id' => $orderId]));

        $payInIds = [];
        foreach ($data['data'] as $payIn) {
            $payInIds[] = $payIn['id'];
        }

        return $payInIds;
    }

    public function refundUncompletedPayments(): void
    {
        $paidAndUncompleted = '?search=status&status=' . TruustPaymentStatus::PUBLISHED;
        $dateEnd = (new DateTime('yesterday'))->format('Y-m-d H:i:s');
        $dateEnd = urlencode($dateEnd);
        $paidAndUncompleted .= '&created_at&&date_end=' . $dateEnd;
        $orders = $this->getOrders($paidAndUncompleted);

        if ($orders) {
            foreach ($orders as $order) {
                $payInIds = $this->getOrderPayInIds($order['id']);
                foreach ($payInIds as $payInId) {
                    $this->refundPayIn($payInId);
                }
            }
        }
    }

    /**
     * @param string $orderId
     * @param float $amount
     * 
     * @return void
     */
    public function reimburseOrder(string $orderId, float $amount = null)
    {
        $body = [];

        if ($amount != null && $amount > 0) {
            $body['amount'] = $amount;
        }

        $response = $this->sendRequest(
            TruustRouter::getRoute(TruustRouter::ROUTE_ORDER_REIMBURSE, [
                'id' => $orderId
            ]),
            $body,
        );

        return $response;
    }
}
