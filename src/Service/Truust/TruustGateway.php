<?php

namespace Dracoder\PaymentBundle\Service\Truust;

use Dracoder\PaymentBundle\Model\BuyerInterface;
use Dracoder\PaymentBundle\Model\PayableInterface;
use Dracoder\PaymentBundle\Model\PayableTokenizerInterface;
use Dracoder\PaymentBundle\Model\PaymentStatus;
use Dracoder\PaymentBundle\Model\SellerInterface;
use Dracoder\PaymentBundle\Service\AbstractTwoStepPaymentGateway;
use Dracoder\PaymentBundle\Service\ManagerInterface;
use RuntimeException;

class TruustGateway extends AbstractTwoStepPaymentGateway
{
    /** @var ManagerInterface|TruustManager $manager */
    protected ManagerInterface $manager;

    /**
     * TruustGateway constructor.
     *
     * @param ManagerInterface $manager
     * @param PayableTokenizerInterface $payableTokenizer
     */
    public function __construct(ManagerInterface $manager, PayableTokenizerInterface $payableTokenizer)
    {
        if (!$manager instanceof TruustManager) {
            throw new RuntimeException('TruustGateway called without a TruustManager');
        }
        parent::__construct($manager, $payableTokenizer);
    }

    /**
     * @param string $order
     *
     * @return bool
     */
    private function refundPayIns(string $order): bool
    {
        $payInIds = $this->manager->getOrderPayInIds($order);

        foreach ($payInIds as $payInId) {
            $this->manager->refundPayIn($payInId);
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function initPayment(
        PayableInterface $payable,
        string $subject,
        string $confirmedCallback,
        string $deniedCallback,
        string $currency = 'EUR'
    ): ?string {
        $this->checkRequirements($payable, $currency);
        $buyer = $payable->getBuyer();
        $seller = $payable->getSeller();
        $this->createBuyer($buyer, $currency);
        $this->createSeller($seller, $currency);
        if ($this->isAllowedToPay($buyer, $seller)) {
            return $this->createTruustOrder($payable, $subject, $confirmedCallback, $deniedCallback);
        }

        return null;
    }

    /**
     * @param PayableInterface $payable
     * @param string $currency
     */
    private function checkRequirements(PayableInterface $payable, string $currency): void
    {
        if ($payable->getPaymentId()) {
            throw new RuntimeException('You can not pay this again');
        }
        if (!$payable->getBuyer()) {
            throw new RuntimeException('The payable have not buyer');
        }
        if (!$payable->getSeller()) {
            throw new RuntimeException('The payable have not seller');
        }
        if ($currency !== 'EUR') {
            throw new RuntimeException('Truust can not be used with other currency than EUR');
        }
    }

    /**
     * @param BuyerInterface|null $buyer
     * @param string $currency
     */
    private function createBuyer(?BuyerInterface $buyer, string $currency): void
    {
        if (!$buyer) {
            return;
        }

        if (!$buyer->getBuyerId() && $buyer->getEmail()) {
            $this->createBuyerId($buyer);
        }

        if (
            $buyer->getBuyerId()
            && $buyer->getBuyerPaymentType() === PaymentMethod::PAYMENT_METHOD_WALLET
            && !$buyer->getBuyerPaymentKey()
        ) {
            $this->createBuyerWallet($buyer, $currency);
        }
    }

    /**
     * @param BuyerInterface $buyer
     */
    private function createBuyerId(BuyerInterface $buyer): void
    {
        $customerId = $this->manager->createCustomer($buyer->getEmail());
        $buyer->setBuyerId($customerId);
    }

    /**
     * @param BuyerInterface $buyer
     * @param string $currency
     */
    private function createBuyerWallet(BuyerInterface $buyer, string $currency): void
    {
        $walletId = $this->manager->createWallet($buyer->getBuyerId(), $currency);
        if ($walletId !== null) {
            $buyer->setBuyerPaymentType(PaymentMethod::PAYMENT_METHOD_WALLET);
            $buyer->setBuyerPaymentKey($walletId);
        }
    }

    /**
     * @param SellerInterface|null $seller
     * @param string $currency
     */
    private function createSeller(?SellerInterface $seller, string $currency): void
    {
        if (!$seller) {
            return;
        }

        if (!$seller->getSellerId() && $seller->getEmail()) {
            $this->createSellerId($seller);
        }

        if (
            $seller->getSellerId()
            && $seller->getSellerPaymentType() === PaymentMethod::PAYMENT_METHOD_WALLET
            && !$seller->getSellerPaymentKey()
        ) {
            $this->createSellerWallet($seller, $currency);
        }
    }

    /**
     * @param SellerInterface $seller
     */
    private function createSellerId(SellerInterface $seller): void
    {
        $customerId = $this->manager->createCustomer($seller->getEmail());
        $seller->setSellerId($customerId);
    }

    /**
     * @param SellerInterface $seller
     * @param string $currency
     */
    private function createSellerWallet(SellerInterface $seller, string $currency): void
    {
        $walletId = $this->manager->createWallet($seller->getSellerId(), $currency);
        if ($walletId !== null) {
            $seller->setSellerPaymentType(PaymentMethod::PAYMENT_METHOD_WALLET);
            $seller->setSellerPaymentKey($walletId);
        }
    }

    /**
     * @param BuyerInterface|null $buyer
     * @param SellerInterface|null $seller
     *
     * @return bool
     */
    private function isAllowedToPay(?BuyerInterface $buyer, ?SellerInterface $seller): bool
    {
        $allowed = true;
        if (!$buyer || !$buyer->getBuyerId() || !$buyer->getBuyerPaymentType()) {
            error_log('Buyer payment info missing');
            $allowed = false;
        }

        if (!$seller || !$seller->getSellerId() || !$seller->getSellerPaymentType()) {
            error_log('Seller payment info missing');
            $allowed = false;
        }

        return $allowed;
    }

    /**
     * @param PayableInterface $payable
     * @param string $subject
     * @param string $confirmedCallback
     * @param string $deniedCallback
     *
     * @return string|null
     */
    private function createTruustOrder(
        PayableInterface $payable,
        string $subject,
        string $confirmedCallback,
        string $deniedCallback
    ): ?string {
        /** @var BuyerInterface $buyer */
        $buyer = $payable->getBuyer();

        /** @var SellerInterface $seller */
        $seller = $payable->getSeller();

        $orderId = $this->manager->createOrder(
            $buyer->getBuyerId(),
            $seller->getSellerId(),
            $payable->getTotal(),
            $subject,
            $confirmedCallback,
            $deniedCallback
        );

        if ($orderId) {
            $payable->setPaymentId($orderId);
            $directUrl = $this->manager->createPayIn(
                $orderId,
                $buyer->getBuyerPaymentType(),
                $buyer->getBuyerPaymentKey()
            );
            $payable->setPaymentStatus(($directUrl ? PaymentStatus::PENDING : PaymentStatus::CANCELED));

            return $directUrl;
        }

        return null;
    }

    /**
     * @param PayableInterface $payable
     * @param string $token
     *
     * @return bool
     */
    public function finishPayment(PayableInterface $payable, string $token): bool
    {
        $orderId = $payable->getPaymentId();
        $seller = $payable->getSeller();

        if ($orderId && $seller && $this->getConfirmedToken($payable) === $token) {
            $payOut = $this->manager->createPayOut(
                $orderId,
                $seller->getSellerPaymentType(),
                $seller->getSellerPaymentKey()
            );

            if ($payOut) {
                $accepted = $this->manager->acceptOrder($orderId);

                if ($accepted) {
                    $validated = $this->manager->validateOrder($orderId);

                    if ($validated) {
                        $payable->setPaymentStatus(PaymentStatus::PAID);

                        return true;
                    }
                }
            }
        }
        $payable->setPaymentStatus(PaymentStatus::CANCELED);
        $this->refundPayIns($orderId);

        return false;
    }

    /**
     * @param PayableInterface $payable
     * 
     * @return void
     */
    public function updatePaymentStatus(PayableInterface $payable)
    {
        $orderFounded = $this->manager->getOrder($payable->getPaymentId());

        if (!$orderFounded) {
            return null;
        }

        $orderStatus = $orderFounded['data']['status'];

        switch ($orderStatus) {
            case TruustPaymentStatus::PENDING_VALIDATE:
            case TruustPaymentStatus::PENDING_RELEASE:
            case TruustPaymentStatus::BLOCKED_RELEASE:
            case TruustPaymentStatus::PENDING_PUBLISH:
            case TruustPaymentStatus::DRAFT:
            case TruustPaymentStatus::ACCEPTED:
            case TruustPaymentStatus::PUBLISHED:
                $payable->setPaymentStatus(PaymentStatus::PENDING);
                break;

            case TruustPaymentStatus::FAILURE:
            case TruustPaymentStatus::CANCELLED:
            case TruustPaymentStatus::REJECTED:
                $payable->setPaymentStatus(PaymentStatus::CANCELED);
                break;

            case TruustPaymentStatus::RELEASED:
                $payable->setPaymentStatus(PaymentStatus::PAID);
                break;
        }
    }

    /**
     * @param PayableInterface $payable
     * @param string $subject
     * @param string $currency
     *
     * @return bool|null
     */
    public function pay(
        PayableInterface $payable,
        string $subject,
        string $currency = 'EUR'
    ): bool {
        /** @var BuyerInterface $buyer */
        $buyer = $payable->getBuyer();

        /** @var SellerInterface $seller */
        $seller = $payable->getSeller();

        $this->createBuyer($buyer, $currency);
        $this->createSeller($seller, $currency);

        if (
            $buyer->getBuyerPaymentType() !== PaymentMethod::PAYMENT_METHOD_WALLET
            || !$buyer->getBuyerPaymentKey()
        ) {
            return false;
        }

        $orderId = $this->manager->createOrder(
            $buyer->getBuyerId(),
            $seller->getSellerId(),
            $payable->getTotal(),
            $subject,
            "",
            ""
        );

        if (!$orderId) {
            return false;
        }

        $directUrl = $this->manager->createPayIn(
            $orderId,
            $buyer->getBuyerPaymentType(),
            $buyer->getBuyerPaymentKey()
        );

        if (!$directUrl) {
            return false;
        }

        $this->manager->sendRequest([
            'method' => 'GET',
            'url' => $directUrl
        ]);

        $payOut = $this->manager->createPayOut(
            $orderId,
            $seller->getSellerPaymentType(),
            $seller->getSellerPaymentKey()
        );

        if ($payOut) {
            $accepted = $this->manager->acceptOrder($orderId);

            if ($accepted) {
                $validated = $this->manager->validateOrder($orderId);

                if ($validated) {
                    $payable->setPaymentId($orderId);
                    $payable->setPaymentStatus(PaymentStatus::PAID);
                    return true;
                }
            }
        }

        $this->refundPayIns($orderId);
        $payable->setPaymentStatus(PaymentStatus::CANCELED);
        return false;
    }

    /**
     * @param string $orderId
     * @param float $amount
     * 
     * @return boolean
     */
    public function reimburseOrder(string $orderId, float $amount = null): bool
    {
        $res = $this->manager->reimburseOrder($orderId, $amount);

        if ($res == null) {
            return true;
        } else {
            return false;
        }
    }
}
