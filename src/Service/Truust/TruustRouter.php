<?php

namespace Dracoder\PaymentBundle\Service\Truust;

use Dracoder\PaymentBundle\Model\AbstractRouter;

class TruustRouter extends AbstractRouter
{
    public const ROUTE_BANK_ACCOUNT_CREATE = 'bank_account_create';
    public const ROUTE_BANK_ACCOUNT_SHOW = 'bank_account_show';
    public const ROUTE_CUSTOMER_CREATE = 'customer_create';
    public const ROUTE_CUSTOMER_BANKS = 'customer_banks';
    public const ROUTE_ORDER_LIST = 'order_list';
    public const ROUTE_ORDER_CREATE = 'order_create';
    public const ROUTE_ORDER_UPDATE = 'order_update';
    public const ROUTE_ORDER_SHOW = 'order_show';
    public const ROUTE_ORDER_ACCEPT = 'order_accept';
    public const ROUTE_ORDER_VALIDATE = 'order_validate';
    public const ROUTE_ORDER_PAYINS = 'order_payins';
    public const ROUTE_ORDER_REIMBURSE = 'order_reimburse';
    public const ROUTE_WALLET_CREATE = 'wallet_create';
    public const ROUTE_PAYIN_LIST = 'payin_list';
    public const ROUTE_PAYIN_SHOW = 'payin_show';
    public const ROUTE_PAYIN_CREATE = 'payin_create';
    public const ROUTE_PAYIN_REFUND = 'payin_refund';
    public const ROUTE_PAYOUT_CREATE = 'payout_create';

    private const ENDPOINTS = [
        self::ROUTE_BANK_ACCOUNT_CREATE => ['method' => 'POST', 'route' => 'bankaccounts'],
        self::ROUTE_BANK_ACCOUNT_SHOW => ['method' => 'GET', 'route' => 'bankaccounts/{id}'],
        self::ROUTE_CUSTOMER_CREATE => ['method' => 'POST', 'route' => 'customers'],
        self::ROUTE_CUSTOMER_BANKS => ['method' => 'GET', 'route' => 'customers/{id}/bankaccounts'],
        self::ROUTE_ORDER_LIST => ['method' => 'GET', 'route' => 'orders{search}'],
        self::ROUTE_ORDER_CREATE => ['method' => 'POST', 'route' => 'orders'],
        self::ROUTE_ORDER_SHOW => ['method' => 'GET', 'route' => 'orders/{id}'],
        self::ROUTE_ORDER_ACCEPT => ['method' => 'POST', 'route' => 'orders/{id}/accept'],
        self::ROUTE_ORDER_UPDATE => ['method' => 'PUT', 'route' => 'orders/{id}'],
        self::ROUTE_ORDER_VALIDATE => ['method' => 'POST', 'route' => 'orders/{id}/validate'],
        self::ROUTE_ORDER_REIMBURSE => ['method' => 'POST', 'route' => 'orders/{id}/reimburse'],
        self::ROUTE_ORDER_PAYINS => ['method' => 'GET', 'route' => 'payins?search=escrow_id&query={id}'],
        self::ROUTE_WALLET_CREATE => ['method' => 'POST', 'route' => 'wallets'],
        self::ROUTE_PAYIN_LIST => ['method' => 'GET', 'route' => 'payins'],
        self::ROUTE_PAYIN_SHOW => ['method' => 'GET', 'route' => 'payins/{id}'],
        self::ROUTE_PAYIN_CREATE => ['method' => 'POST', 'route' => 'payins'],
        self::ROUTE_PAYIN_REFUND => ['method' => 'POST', 'route' => 'payins/{id}/refund'],
        self::ROUTE_PAYOUT_CREATE => ['method' => 'POST', 'route' => 'payouts'],
    ];

    /**
     * @param string $routeName
     *
     * @return string[]
     */
    protected static function getEndpoints(string $routeName): array
    {
        return self::ENDPOINTS[$routeName];
    }
}
