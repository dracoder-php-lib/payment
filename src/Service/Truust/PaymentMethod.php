<?php

namespace Dracoder\PaymentBundle\Service\Truust;

class PaymentMethod
{
    public const PAYMENT_METHOD_WALLET = 'WALLET';
    public const PAYMENT_METHOD_REDSYS = 'REDSYS_V2';
    public const PAYMENT_METHOD_BANK_WIRE = 'BANKWIRE';
    public const PAYMENT_METHOD_BANK_ACCOUNT = 'ACCOUNT';
    private const SUPPORTED_PAYIN_TYPES = [
        self::PAYMENT_METHOD_WALLET => 'wallet_id',
        self::PAYMENT_METHOD_REDSYS => null,
        self::PAYMENT_METHOD_BANK_WIRE => null,
    ];
    private const SUPPORTED_PAYOUT_TYPES = [
        self::PAYMENT_METHOD_WALLET => 'wallet_id',
        self::PAYMENT_METHOD_BANK_ACCOUNT => 'bankaccount_id',
    ];

    /**
     * @param string $orderId
     * @param string $type
     * @param string|null $id
     *
     * @return array|null
     */
    public static function getPayInBody(string $orderId, string $type, ?string $id = null): ?array
    {
        return self::getPaymentBody($orderId, self::SUPPORTED_PAYIN_TYPES, $type, $id);
    }

    /**
     * @param string $orderId
     * @param string $type
     * @param string|null $id
     *
     * @return array|null
     */
    public static function getPayOutBody(string $orderId, string $type, ?string $id = null): ?array
    {
        return static::getPaymentBody($orderId, self::SUPPORTED_PAYOUT_TYPES, $type, $id);
    }

    /**
     * @param string $orderId
     * @param array $supportedPaymentMethod
     * @param string $paymentMethod
     * @param string|null $id
     *
     * @return array|null
     */
    private static function getPaymentBody(
        string $orderId,
        array $supportedPaymentMethod,
        string $paymentMethod,
        ?string $id = null
    ): ?array {
        if (!array_key_exists($paymentMethod, $supportedPaymentMethod)) {
            return null;
        }
        $paramName = $supportedPaymentMethod[$paymentMethod];
        if ($paramName && !$id) {
            return null;
        }
        $paymentBody = ['order_id' => $orderId, 'type' => $paymentMethod];
        if ($paramName) {
            $paymentBody[$paramName] = $id;
        }

        return $paymentBody;
    }
}
