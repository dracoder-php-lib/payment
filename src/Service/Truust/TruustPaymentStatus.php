<?php

namespace Dracoder\PaymentBundle\Service\Truust;

class TruustPaymentStatus
{
    public const PENDING_VALIDATE = 'PENDING_VALIDATE';
    public const PENDING_RELEASE = 'PENDING_RELEASE';
    public const BLOCKED_RELEASE = 'BLOCKED_RELEASE';
    public const PENDING_PUBLISH = 'PENDING_PUBLISH';
    public const DRAFT = 'DRAFT';
    public const FAILURE = 'FAILURE';
    public const CANCELLED = 'CANCELLED';
    public const REJECTED = 'REJECTED';
    public const PUBLISHED = 'PUBLISHED';
    public const ACCEPTED = 'ACCEPTED';
    public const RELEASED = 'RELEASED';
}
