<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class RedsysManager
{
    private const API_DEBUG = 'https://sis-t.redsys.es:25443/sis/rest/';
    private const API_PROD = 'https://sis.redsys.es/sis/rest/';
    private string $privateKey;
    private Client $client;

    /**
     * TruustManager constructor.
     *
     * @param string $privateKey
     * @param bool $isDebug
     */
    public function __construct(string $privateKey, bool $isDebug = false)
    {
        $this->privateKey = $privateKey;
        if ($isDebug) {
            $this->client = new Client(['base_uri' => self::API_DEBUG, 'verify' => false]);
        } else {
            $this->client = new Client(['base_uri' => self::API_PROD]);
        }
    }

    /**
     * @param array $route
     * @param array $body
     *
     * @return mixed
     */
    private function sendRequest(array $route, array $body = []): ?array
    {
        try {
            $response = $this->client->request(
                $route['method'],
                $route['url'],
                [
                    'headers' => $this->getHeaders(),
                    'form_params' => $body,
                ]
            );
            if ($response) {
                return json_decode(
                    $response->getBody()->__toString(),
                    true,
                    512,
                    JSON_THROW_ON_ERROR
                );
            }
            error_log($route['url'] . ' - No response');
        } catch (GuzzleException $e) {
            error_log($route['url'] . ' - (GuzzleException) '. $e->getMessage());
        } catch (JsonException $e) {
            error_log($route['url'] . ' - (JsonException) '. $e->getMessage());
        }

        return null;
    }

    /**
     * @param array $append
     *
     * @return array
     */
    private function getHeaders(array $append = []): array
    {
        return array_merge(
            [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer '. $this->privateKey,
            ],
            $append
        );
    }
}
