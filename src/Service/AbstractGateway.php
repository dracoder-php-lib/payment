<?php

namespace Dracoder\PaymentBundle\Service;

abstract class AbstractGateway implements GatewayInterface
{
    protected ManagerInterface $manager;

    /**
     * @param ManagerInterface $manager
     */
    public function __construct(ManagerInterface $manager)
    {
        $this->manager = $manager;
    }
}
