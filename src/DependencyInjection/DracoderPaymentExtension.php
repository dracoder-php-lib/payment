<?php

namespace Dracoder\PaymentBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\HttpKernel\KernelInterface;

class DracoderPaymentExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container): void
    {

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yaml');
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $paymentTruustDefinition = $container->getDefinition('payment.truust');
        $apiKey = (isset($config['truust']['api_key']) ? $config['truust']['apiKey'] : 'your_api_key');
        $paymentTruustDefinition->addArgument($apiKey);
        $isDebug = (isset($config['truust']['is_debug']) ? $config['truust']['isDebug'] : true);
        $paymentTruustDefinition->addArgument($isDebug);

        $payableTokenizerDefinition = $container->getDefinition('payable.tokenizer');
        $confirmedSecret = ($config['tokenizer']['confirmed_secret'] ?? 'C0nF1Rm3D!');
        $payableTokenizerDefinition->addArgument($confirmedSecret);
        $deniedSecret = ($config['tokenizer']['denied_secret'] ?? 'd3N130#');
        $payableTokenizerDefinition->addArgument($deniedSecret);
    }

    public function getAlias(): string
    {
        return 'dracoder_payment';
    }
}
