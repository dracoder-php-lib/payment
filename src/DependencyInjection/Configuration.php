<?php

namespace Dracoder\PaymentBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('dracoder_payment');
        $treeBuilder->getRootNode()
            ->children()
                ->arrayNode('truust')
                    ->children()
                        ->scalarNode('api_key')->defaultValue('your_api_key')->end()
                        ->booleanNode('is_debug')->defaultTrue()->end()
                    ->end()
                ->end()
                ->arrayNode('tokenizer')
                    ->children()
                        ->scalarNode('confirmed_secret')->defaultValue('C0nF1Rm3D!')->end()
                        ->scalarNode('denied_secret')->defaultValue('d3N130#')->end()
                    ->end()
                ->end()
            ->end()
        ->end();

        return $treeBuilder;
    }
}
