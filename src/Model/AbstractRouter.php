<?php

namespace Dracoder\PaymentBundle\Model;

abstract class AbstractRouter
{
    abstract protected static function getEndpoints(string $routeName): array;

    /**
     * @param string $routeName
     * @param array $params
     *
     * @return array
     */
    public static function getRoute(string $routeName, array $params = []): array
    {
        $endpoint = static::getEndpoints($routeName);
        $url = $endpoint['route'];
        foreach ($params as $name => $value) {
            $url = str_replace('{'.$name.'}', $value, $url);
        }

        return ['method' => $endpoint['method'], 'url' => $url];
    }
}
