<?php

namespace Dracoder\PaymentBundle\Model;

class AbstractSeller extends AbstractCustomer implements SellerInterface
{
    private ?string $sellerId = null;
    private ?string $sellerPaymentType = null;
    private ?string $sellerPaymentKey = null;

    /**
     * @inheritDoc
     */
    public function getSellerId(): ?string
    {
        return $this->sellerId;
    }

    /**
     * @inheritDoc
     */
    public function setSellerId(?string $sellerId): self
    {
        $this->sellerId = $sellerId;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getSellerPaymentType(): ?string
    {
        return $this->sellerPaymentType;
    }

    /**
     * @inheritDoc
     */
    public function setSellerPaymentType(?string $paymentType): self
    {
        $this->sellerPaymentType = $paymentType;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getSellerPaymentKey(): ?string
    {
        return $this->sellerPaymentKey;
    }

    /**
     * @inheritDoc
     */
    public function setSellerPaymentKey(?string $paymentKey): self
    {
        $this->sellerPaymentKey = $paymentKey;

        return $this;
    }
}
