<?php

namespace Dracoder\PaymentBundle\Model;

interface CustomerInterface
{
    /**
     * @return string|null
     */
    public function getEmail(): ?string;
}
