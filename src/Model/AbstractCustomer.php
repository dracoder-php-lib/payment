<?php

namespace Dracoder\PaymentBundle\Model;

abstract class AbstractCustomer implements CustomerInterface
{
    private ?string $email = null;

    /**
     * @inheritDoc
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }
}
