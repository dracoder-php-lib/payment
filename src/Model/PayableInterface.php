<?php

namespace Dracoder\PaymentBundle\Model;

use DateTimeInterface;

interface PayableInterface
{
    /**
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * @return BuyerInterface|null
     */
    public function getBuyer(): ?BuyerInterface;

    /**
     * @return SellerInterface|null
     */
    public function getSeller(): ?SellerInterface;

    /**
     * @return string|null
     */
    public function getPaymentId(): ?string;

    /**
     * @param string|null $paymentId
     *
     * @return $this
     */
    public function setPaymentId(?string $paymentId): self;

    /**
     * @return string|null
     */
    public function getTotal(): ?string;

    /**
     * @return string|null
     */
    public function getPaymentStatus(): ?string;

    /**
     * @param string|null $paymentStatus
     *
     * @return $this
     */
    public function setPaymentStatus(?string $paymentStatus): self;

    /**
     * @return ?string
     */
    public function getSalt(): ?string;

    /**
     * @return DateTimeInterface
     */
    public function getCreatedAt(): ?DateTimeInterface;
}
