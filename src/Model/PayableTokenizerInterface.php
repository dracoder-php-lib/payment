<?php

namespace Dracoder\PaymentBundle\Model;

interface PayableTokenizerInterface
{
    /**
     * @param PayableInterface $payable
     *
     * @return string
     */
    public function getConfirmedToken(PayableInterface $payable): string;

    /**
     * @param PayableInterface $payable
     *
     * @return string
     */
    public function getDeniedToken(PayableInterface $payable): string;
}
