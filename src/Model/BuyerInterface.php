<?php

namespace Dracoder\PaymentBundle\Model;

interface BuyerInterface extends CustomerInterface
{
    /**
     * @return string|null
     */
    public function getBuyerId(): ?string;

    /**
     * @param string|null $buyerId
     *
     * @return $this
     */
    public function setBuyerId(?string $buyerId): self;

    /**
     * @return string|null
     */
    public function getBuyerPaymentType(): ?string;

    /**
     * @param string|null $paymentType
     *
     * @return $this
     */
    public function setBuyerPaymentType(?string $paymentType): self;

    /**
     * @return string|null
     */
    public function getBuyerPaymentKey(): ?string;

    /**
     * @param string|null $paymentKey
     *
     * @return $this
     */
    public function setBuyerPaymentKey(?string $paymentKey): self;
}
