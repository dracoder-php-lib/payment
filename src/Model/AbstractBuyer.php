<?php

namespace Dracoder\PaymentBundle\Model;

class AbstractBuyer extends AbstractCustomer implements BuyerInterface
{
    private ?string $buyerId = null;
    private ?string $buyerPaymentType = null;
    private ?string $buyerPaymentKey = null;

    /**
     * @inheritDoc
     */
    public function getBuyerId(): ?string
    {
        return $this->buyerId;
    }

    /**
     * @inheritDoc
     */
    public function setBuyerId(?string $buyerId): self
    {
        $this->buyerId = $buyerId;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getBuyerPaymentType(): ?string
    {
        return $this->buyerPaymentType;
    }

    /**
     * @inheritDoc
     */
    public function setBuyerPaymentType(?string $paymentType): self
    {
        $this->buyerPaymentType = $paymentType;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getBuyerPaymentKey(): ?string
    {
        return $this->buyerPaymentKey;
    }

    /**
     * @inheritDoc
     */
    public function setBuyerPaymentKey(?string $paymentKey): self
    {
        $this->buyerPaymentKey = $paymentKey;

        return $this;
    }
}
