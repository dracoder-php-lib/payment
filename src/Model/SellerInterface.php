<?php

namespace Dracoder\PaymentBundle\Model;

interface SellerInterface extends CustomerInterface
{
    /**
     * @return string|null
     */
    public function getSellerId(): ?string;

    /**
     * @param string|null $sellerId
     *
     * @return $this
     */
    public function setSellerId(?string $sellerId): self;

    /**
     * @return string|null
     */
    public function getSellerPaymentType(): ?string;

    /**
     * @param string|null $paymentType
     *
     * @return $this
     */
    public function setSellerPaymentType(?string $paymentType): self;

    /**
     * @return string|null
     */
    public function getSellerPaymentKey(): ?string;

    /**
     * @param string|null $paymentKey
     *
     * @return $this
     */
    public function setSellerPaymentKey(?string $paymentKey): self;
}
