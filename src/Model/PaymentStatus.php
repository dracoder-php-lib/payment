<?php

namespace Dracoder\PaymentBundle\Model;

class PaymentStatus
{
    public const PENDING = 'pending';
    public const PAID = 'paid';
    public const CANCELED = 'canceled';
    public const REIMBURSED = 'reimbursed';
}
