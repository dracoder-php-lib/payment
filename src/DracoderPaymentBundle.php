<?php

namespace Dracoder\PaymentBundle;

use Dracoder\PaymentBundle\DependencyInjection\DracoderPaymentExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class DracoderPaymentBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new DracoderPaymentExtension();
    }
}
