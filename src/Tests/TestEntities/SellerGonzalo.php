<?php

namespace Dracoder\PaymentBundle\Tests\TestEntities;

use Dracoder\PaymentBundle\Model\AbstractSeller;
use Dracoder\PaymentBundle\Service\Truust\PaymentMethod;

class SellerGonzalo extends AbstractSeller
{
    public function getEmail(): ?string
    {
        return 'gonzalo@stampymail.com';
    }

    /**
     * @return string|null
     */
    public function getSellerPaymentType(): ?string
    {
        return PaymentMethod::PAYMENT_METHOD_WALLET;
    }
}
