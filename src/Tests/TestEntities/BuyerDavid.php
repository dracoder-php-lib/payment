<?php

namespace Dracoder\PaymentBundle\Tests\TestEntities;

use Dracoder\PaymentBundle\Model\AbstractBuyer;
use Dracoder\PaymentBundle\Service\Truust\PaymentMethod;

class BuyerDavid extends AbstractBuyer
{
    public function getEmail(): ?string
    {
        return 'david@stampymail.com';
    }

    /**
     * @return string|null
     */
    public function getBuyerPaymentType(): ?string
    {
        return PaymentMethod::PAYMENT_METHOD_WALLET;
    }
}
