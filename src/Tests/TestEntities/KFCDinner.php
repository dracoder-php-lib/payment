<?php

namespace Dracoder\PaymentBundle\Tests\TestEntities;

use DateTime;
use DateTimeInterface;
use Dracoder\PaymentBundle\Model\BuyerInterface;
use Dracoder\PaymentBundle\Model\PayableInterface;
use Dracoder\PaymentBundle\Model\SellerInterface;

class KFCDinner implements PayableInterface
{
    private ?string $paymentId = null;
    private ?string $paymentStatus = null;
    private BuyerInterface $buyer;
    private SellerInterface $seller;

    public function __construct()
    {
        $this->buyer = new BuyerDavid();
        $this->seller = new SellerGonzalo();
    }

    /**
     * @inheritDoc
     */
    public function getBuyer(): ?BuyerInterface
    {
        return $this->buyer;
    }

    /**
     * @inheritDoc
     */
    public function getSeller(): ?SellerInterface
    {
        return $this->seller;
    }

    /**
     * @inheritDoc
     */
    public function getPaymentId(): ?string
    {
        return $this->paymentId;
    }

    /**
     * @inheritDoc
     */
    public function setPaymentId(?string $paymentId): self
    {
        $this->paymentId = $paymentId;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getTotal(): ?string
    {
        return '35.79';
    }

    /**
     * @inheritDoc
     */
    public function getPaymentStatus(): ?string
    {
        return $this->paymentStatus;
    }

    /**
     * @inheritDoc
     */
    public function setPaymentStatus(?string $paymentStatus): self
    {
        $this->paymentStatus = $paymentStatus;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getSalt(): ?string
    {
        return 'algo';
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt(): ?DateTimeInterface
    {
        return DateTime::createFromFormat('Y-m-d H:i:s', '2020-12-03 16:38:22');
    }
}
