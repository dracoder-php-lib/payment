<?php

namespace Dracoder\PaymentBundle\Tests;

use Dracoder\PaymentBundle\Model\PayableInterface;
use Dracoder\PaymentBundle\Service\ManagerInterface;
use Dracoder\PaymentBundle\Service\PayableTokenizer;
use Dracoder\PaymentBundle\Service\Truust\TruustGateway;
use Dracoder\PaymentBundle\Service\Truust\TruustManager;
use Dracoder\PaymentBundle\Service\TwoStepPaymentGatewayInterface;
use Dracoder\PaymentBundle\Tests\TestEntities\KFCDinner;

class TestController
{
    private ManagerInterface $manager;
    private TwoStepPaymentGatewayInterface $gateway;

    public function __construct()
    {
        $this->manager = new TruustManager('sk_stage_DotYVAaLwWbEBJFEglqXJiDb', true);
        $this->gateway = new TruustGateway($this->manager, new PayableTokenizer('ok', 'nok'));
    }

    public function createPayment(): void
    {
        $baseUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
                "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .
            $_SERVER['REQUEST_URI'];


        $payable = new KFCDinner();

        $gatewayUrl = $this->gateway->initPayment(
            $payable,
            'Cena en el KFC',
            $baseUrl.'?result=confirmed',
            $baseUrl.'?result=canceled',
            'EUR'
        );

        if ($gatewayUrl) {
            echo $gatewayUrl;
            $this->savePayment($payable);
        } else {
            echo 'Ha habido algún problema al procesar el pago';
        }
        echo '<br/> Datos del objeto que se intentaba pagar: ';

        var_dump($payable);
    }

    public function confirmed(): void
    {
        $payable = $this->loadPayment();
        $this->gateway->finishPayment($payable, $this->gateway->getConfirmedToken($payable));
        var_dump($payable);
    }

    public function canceled(): void
    {
        $payable = $this->loadPayment();
        $this->gateway->finishPayment($payable, $this->gateway->getDeniedToken($payable));

        var_dump($payable);
    }

    /**
     * @param KFCDinner $kfcDinner
     */
    private function savePayment(KFCDinner $kfcDinner): void
    {
        file_put_contents('payable', serialize($kfcDinner));
    }

    /**
     * @return PayableInterface
     */
    private function loadPayment(): PayableInterface
    {
        return unserialize(file_get_contents('payable'));
    }

    public function refundUncompleted(): void
    {
        $this->manager->refundUncompletedPayments();
    }
}
