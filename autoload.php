<?php

spl_autoload_register(function ($classname) {
    if (startsWith($classname, 'Dracoder\\PaymentBundle')) {
        $filename = str_replace('Dracoder\\PaymentBundle', '', $classname);
        $filename = str_replace('\\', '/', $filename) . '.php';

        include './src/' . $filename;
    }
});

function startsWith ($haystack, $needle)
{
    $len = strlen($needle);
    return (strpos($haystack, $needle) === 0);
}
